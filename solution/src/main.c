#include "../include/bmp_image.h"
#include "../include/file_IO.h"
#include "../include/rotate.h"
#include <stdio.h>


int main( int argc, char** argv ) {
     if (argc == 3) {

    // Messages with results of reading
    const char* open_error_msgs[] = {
        [OPEN_OK] = "File successfully opened!",
        [OPEN_ERROR] = "Error! File is not open!"
    };

    // Messages with results of closing
    const char* close_error_msgs[] = {
        [CLOSE_OK] = "File successfully closed!",
        [CLOSE_ERROR] = "Error! File is not closed!"
    };

    // Messages with results of reading
    const char* read_error_msgs[] = {
        [READ_OK] = "File was read successfully!",
        [READ_INVALID_DATA] = "Error with pixel array! File is not read!",
        [READ_INVALID_HEADER] = "Error with header! File is not read!",
        [READ_INVALID_SIGNATURE] = "Error in signature! File is not read!"
    };

    // Messages with results of writing
    const char* write_error_msgs[] = {
        [WRITE_OK] = "File was written successfully!",
        [WRITE_INVALID_DATA] = "Error with pixel array! File is not written!",
        [WRITE_INVALID_HEADER] = "Error with header! File is not written!",
    };

    FILE* input;
    FILE* output;
    printf("%s\n", open_error_msgs[open_file(&input, argv[1], R)]);
    struct image img = {0};
    printf("%s\n", read_error_msgs[from_bmp(input, &img)]);
    printf("%s\n", close_error_msgs[close_file(input)]);    
    printf("%s\n", open_error_msgs[open_file(&output, argv[2], W)]);
    struct image img_rotated = {0};
    rotate_counterclockwise(&img, &img_rotated);
    printf("%s\n", write_error_msgs[to_bmp(output, &img_rotated)]);
    printf("%s\n", close_error_msgs[close_file(output)]);
    struct_image_free(&img);
    struct_image_free(&img_rotated);
    return 0;
     }
     else{
        printf("Bad usage! Your command should look like\n./image-transformer <source-image> <transformed-image>\n");
        return 1;
     }
}
