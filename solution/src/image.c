#include "../include/image.h"

void struct_image_create(struct image* image, uint64_t width, uint64_t height){
    image->height = height;
    image->width = width;
    image->data = malloc(height * (width * sizeof(struct pixel) + width%4));
}

uint64_t get_pixel_address(const uint64_t width, uint64_t i, uint64_t j) {
    return i*width+ j;
}

void struct_image_free(struct image* image){
    free(image->data);
}
