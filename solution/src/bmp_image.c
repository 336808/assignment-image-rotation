#include "../include/bmp_image.h"

// BMP header structure
struct __attribute__((packed)) bmp_header 
{
    uint16_t  bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};


enum read_status from_bmp( FILE* const in, struct image* img ){
    struct bmp_header header;
    if (fread(&header, sizeof(struct bmp_header), 1, in) < 1) return READ_INVALID_HEADER;
    struct_image_create(img, header.biWidth, header.biHeight);

    const uint32_t padding = img->width%4;
    for (uint32_t i = 0; i < img->height; i++) {
        if(fread(img->data + i * img->width, sizeof(struct pixel), img->width, in) < img->width){
            struct_image_free(img);
            return READ_INVALID_DATA;
        }
        if (fseek(in, padding, SEEK_CUR) != 0) {
            struct_image_free(img);
            return READ_INVALID_SIGNATURE;
        }
    }
    return READ_OK;
}

struct bmp_header generate_header(const struct image* img, uint32_t const padding){
    const struct bmp_header header = {
        .bfType = 19778,
        .bfileSize = sizeof(struct bmp_header) + (3 * img->width + padding) * img->height,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = 40,
        .biWidth = img->width,
        .biHeight = img->height,
        .biPlanes = 1,
        .biBitCount = 24,
        .biSizeImage = (3 * img->width + padding) * img->height
    };

    return header;
}

enum write_status to_bmp( FILE* out, struct image const* img ){
    const uint32_t padding = img->width%4;
    
    const struct bmp_header header = generate_header(img, padding);
    
    if(fwrite(&header, sizeof(struct bmp_header), 1, out) < 1) return WRITE_INVALID_HEADER;
    
    const char trash_pixels[] = {0, 0, 0}; 
    for (uint32_t i = 0; i < img->height; i++) {
        if(fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out) < img->width ||
           fwrite(trash_pixels, 1, padding, out) < 1){
                return WRITE_INVALID_DATA;
           }
    }
    return WRITE_OK;
}
