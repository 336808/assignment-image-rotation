#include "../include/rotate.h"


void rotate_counterclockwise(const struct image* img, struct image* res){
    struct_image_create(res, img->height, img->width);
    for (uint64_t i = 0; i < img->height; i++) {
        for (uint64_t j = 0; j < img->width; j++) {
            res->data[get_pixel_address(res->width, j, res->width-i-1)] = img->data[get_pixel_address(img->width, i, j)];
        }
    }
}

