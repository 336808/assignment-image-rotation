#include "../include/file_IO.h"


int file_format_(const enum file_format format, char* str) {
  if (format == R){
    str[0] = 'r';
    str[1] = 'b';
    return 1;
  }
  else if (format == W){
    str[0] = 'w';
    str[1] = 'b';
    return 1;
  }
  else{
    return 0;
  }
}

enum open_status open_file( FILE** file, const char* filename, const enum file_format format){
    char str[] = "er";
    char* ptr = str;
    int error = file_format_(format, ptr);
    if (error == 0){
        return OPEN_ERROR;
    }
    *file = fopen(filename, str);
    if (!file) return OPEN_ERROR;
    return OPEN_OK;
}

enum close_status close_file( FILE* file){
    if(fclose(file) == EOF) return CLOSE_ERROR;
    return CLOSE_OK;
}
