#ifndef _ROTATEH_
#define _ROTATEH_
#include "image.h"

void rotate_counterclockwise(const struct image* img, struct image* res);

#endif
