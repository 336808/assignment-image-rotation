#ifndef _IMAGEH_
#define _IMAGEH_
#include <inttypes.h>
#include <malloc.h>
#include <stdio.h>

// Pixel structure (RGB)
struct pixel {
    uint8_t r,g,b;
};

uint64_t get_pixel_address(const uint64_t width, uint64_t i, uint64_t j);

// "Pure" image structure (only width, height and array of pixels)
struct image {
  uint64_t width, height;
  struct pixel* data;
};

void struct_image_create(struct image* image, uint64_t width, uint64_t height);
void struct_image_free(struct image* image);
#endif
