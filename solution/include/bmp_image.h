#ifndef _BMP_IMAGEH_
#define _BMP_IMAGEH_
#include "image.h"


// File reading statuses
enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_DATA,
  READ_INVALID_HEADER
  };
// Creating "pure" image from BMP file
enum read_status from_bmp( FILE* const in, struct image* img );

// File writing statuses
enum  write_status  {
  WRITE_OK = 0,
  WRITE_INVALID_HEADER,
  WRITE_INVALID_DATA
};
// Creating BMP file from "pure" image
enum write_status to_bmp( FILE* out, struct image const* img );


#endif
