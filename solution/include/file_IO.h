#ifndef _FILE_IOH_
#define _FILE_IOH_
#include <inttypes.h>
#include <stdio.h>


enum file_format {
    W,
    R
};

int file_format_(const enum file_format format, char* str);

// File opening statuses
enum open_status  {
  OPEN_OK = 0,
  OPEN_ERROR
  };

enum open_status open_file( FILE** file, const char* filename, const enum file_format format);

// File closing statuses
enum  close_status  {
  CLOSE_OK = 0,
  CLOSE_ERROR
};

enum close_status close_file( FILE* file );


#endif
